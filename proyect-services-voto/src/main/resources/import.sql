INSERT INTO SST_USUARIO (USER_NAME,COD_PERSONAL,DES_PASSWORD,IND_VOTO,IND_BAJA) VALUES ('CESAR22','0001','$2a$10$GeALraV5f4bsnA7crG17yOPKey23O3ElYLod3M1PjFuavbrSijr9C','N','N');
INSERT INTO SST_USUARIO (USER_NAME,COD_PERSONAL,DES_PASSWORD,IND_VOTO,IND_BAJA) VALUES ('ARTURO05','0002','$2a$10$GeALraV5f4bsnA7crG17yOPKey23O3ElYLod3M1PjFuavbrSijr9C','N','N');
INSERT INTO SST_USUARIO (USER_NAME,COD_PERSONAL,DES_PASSWORD,IND_VOTO,IND_BAJA) VALUES ('DARCY28','0003','$2a$10$GeALraV5f4bsnA7crG17yOPKey23O3ElYLod3M1PjFuavbrSijr9C','N','N');

INSERT INTO SST_ROLE (DES_ROLE) VALUES('ROLE_ADMIN');
INSERT INTO SST_ROLE (DES_ROLE) VALUES ('ROLE_USER');

INSERT INTO usuario_role (usuario_id,role_id) VALUES (1,1);
INSERT INTO usuario_role (usuario_id,role_id) VALUES (2,1);
INSERT INTO usuario_role (usuario_id,role_id) VALUES (3,2);

INSERT INTO SST_PERSONA (DES_NOMBRE,DES_APELLIDO,EDAD,IND_BAJA) VALUES('CESAR','MARCOS','20','N');
INSERT INTO SST_PERSONA (DES_NOMBRE,DES_APELLIDO,EDAD,IND_BAJA) VALUES('ARTURO','DAVILA','20','N');
INSERT INTO SST_PERSONA (DES_NOMBRE,DES_APELLIDO,EDAD,IND_BAJA) VALUES('ANGEL','RAMOS','20','N');
INSERT INTO SST_PERSONA (DES_NOMBRE,DES_APELLIDO,EDAD,IND_BAJA) VALUES('DARCY','QUISPE','20','N');
INSERT INTO SST_PERSONA (DES_NOMBRE,DES_APELLIDO,EDAD,IND_BAJA) VALUES('DANIEL','PALACIOS','20','N');

INSERT INTO SST_CANDIDATO (DES_CANDIDATO,DES_AGENCIA,DES_DNI,IMAGEN,USER_ADD,IND_BAJA,FEC_ADD) VALUES ('MIGUEL MORENO','LIMA','12345678','avatar.jpg','GMARCOS','N', to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO SST_CANDIDATO (DES_CANDIDATO,DES_AGENCIA,DES_DNI,IMAGEN,USER_ADD,IND_BAJA,FEC_ADD) VALUES ('MIGUEL MORENO','LIMA','12345677','avatar.jpg','GMARCOS','N', to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO SST_CANDIDATO (DES_CANDIDATO,DES_AGENCIA,DES_DNI,IMAGEN,USER_ADD,IND_BAJA,FEC_ADD) VALUES ('MIGUEL MORENO','LIMA','12345676','avatar.jpg','GMARCOS','N', to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO SST_CANDIDATO (DES_CANDIDATO,DES_AGENCIA,DES_DNI,IMAGEN,USER_ADD,IND_BAJA,FEC_ADD) VALUES ('MIGUEL MORENO','LIMA','12345675','avatar.jpg','GMARCOS','N', to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss'));

INSERT INTO SST_VOTO(COD_CANDIDATO,USER_ADD,DES_AGENCIA,IND_BAJA,COD_PERSONAL,FEC_ADD) VALUES(1,'GMARCOS','LIMA','S','0001',to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss')); 
INSERT INTO SST_VOTO(COD_CANDIDATO,USER_ADD,DES_AGENCIA,IND_BAJA,COD_PERSONAL,FEC_ADD) VALUES(1,'GMARCOS','LIMA','S','0001',to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO SST_VOTO(COD_CANDIDATO,USER_ADD,DES_AGENCIA,IND_BAJA,COD_PERSONAL,FEC_ADD) VALUES(1,'GMARCOS','LIMA','S','0001',to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO SST_VOTO(COD_CANDIDATO,USER_ADD,DES_AGENCIA,IND_BAJA,COD_PERSONAL,FEC_ADD) VALUES(1,'GMARCOS','LIMA','S','0001',to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO SST_VOTO(COD_CANDIDATO,USER_ADD,DES_AGENCIA,IND_BAJA,COD_PERSONAL,FEC_ADD) VALUES(1,'GMARCOS','LIMA','S','0001',to_date('24-06-2019 14:53:28', 'dd-mm-yyyy hh24:mi:ss'));


/* Creamos algunos usuarios con sus roles */
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('andres','$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq',1, 'Andres', 'Guzman','profesor@bolsadeideas.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$RmdEsvEfhI7Rcm9f/uZXPebZVCcPC7ZXZwV51efAvMAp1rIaRAfPK',1, 'John', 'Doe','jhon.doe@bolsadeideas.com');

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);