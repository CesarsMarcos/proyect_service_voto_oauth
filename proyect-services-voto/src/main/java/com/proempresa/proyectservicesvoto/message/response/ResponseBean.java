package com.proempresa.proyectservicesvoto.message.response;

import org.springframework.http.HttpStatus;

public class ResponseBean {

	private String message;
	private HttpStatus status;
	private Object objeto;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

	public ResponseBean(String message, HttpStatus status, Object objeto) {
		super();
		this.message = message;
		this.status = status;
		this.objeto = objeto;
	}
	
	
	

}
