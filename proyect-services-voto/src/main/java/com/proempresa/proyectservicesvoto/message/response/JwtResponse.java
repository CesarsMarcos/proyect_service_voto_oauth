package com.proempresa.proyectservicesvoto.message.response;


public class JwtResponse {
	/*
	private HttpStatus status;
	private String CodAuxiliar;
	private String nomColaborador;
	private String email;
 	*/
	private String token;
    private String type = "Bearer";
    
    
    
    public JwtResponse(String accessToken) {
        this.token = accessToken;
    }
 
    public String getAccessToken() {
        return token;
    }
 
    public void setAccessToken(String accessToken) {
        this.token = accessToken;
    }
 
    public String getTokenType() {
        return type;
    }
 
    public void setTokenType(String tokenType) {
        this.type = tokenType;
    }
	

	
	
	
	
	
}
