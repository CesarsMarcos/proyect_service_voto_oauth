package com.proempresa.proyectservicesvoto.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.proempresa.proyectservicesvoto.bean.SstPersona;
import com.proempresa.proyectservicesvoto.message.response.ResponseBean;
import com.proempresa.proyectservicesvoto.repository.PersonaJpaRepository;


@RestController
@RequestMapping("/api/")
@CrossOrigin(origins="*" ,methods= {RequestMethod.GET,RequestMethod.POST})
public class PersonaController {

	private static final Logger LOG = LoggerFactory.getLogger(PersonaController.class);

	@Autowired
	PersonaJpaRepository personaRepository;

	@GetMapping("/persona")
	@Transactional(readOnly = true)
	public ResponseEntity<?> list() {
		LOG.info("[PersonaController]");
		List<SstPersona> persona = new ArrayList<SstPersona>();
		try {
			persona = personaRepository.findAll();
		} catch (DataAccessException e) {
			LOG.info("[PersonaController] " + e.getMessage().concat(": ").concat(e.getLocalizedMessage()));
			return new ResponseEntity<List<SstPersona>>(persona, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (persona == null) {
			LOG.info("[PersonaController]");
			return new ResponseEntity<List<SstPersona>>(persona,HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<SstPersona>>(persona, HttpStatus.OK);
	}

	@GetMapping("/persona/{cod}")
	public ResponseEntity<?> get(@PathVariable long codPersona) {
		SstPersona persona = new SstPersona();
		try {
			persona = personaRepository.findByCodPersona(codPersona);
		} catch (DataAccessException e) {
			LOG.error(e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<ResponseBean>(new ResponseBean("un error ha ocurrido", HttpStatus.INTERNAL_SERVER_ERROR, persona),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (persona == null) {
			return new ResponseEntity<ResponseBean>(new ResponseBean("La persona con el codigo "+codPersona+" no existe.", HttpStatus.NOT_FOUND, persona),
					HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ResponseBean>(new ResponseBean("", HttpStatus.OK, persona), HttpStatus.OK);
	}

	@PostMapping("/persona")
	@Transactional
	public ResponseEntity<?> save(@RequestBody SstPersona personaNew) {

		if (personaNew == null) {
			return new ResponseEntity<ResponseBean>(new ResponseBean("",HttpStatus.BAD_REQUEST, personaNew) ,HttpStatus.BAD_REQUEST);
		}
		try {
			personaRepository.save(personaNew);
		} catch (DataAccessException e) {
			return new ResponseEntity<ResponseBean>(
					new ResponseBean("", HttpStatus.INTERNAL_SERVER_ERROR, personaNew),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ResponseBean>(new ResponseBean("", HttpStatus.CREATED, personaNew),
				HttpStatus.CREATED);
	}

	@PutMapping("/persona/{id}")
	@Transactional
	public ResponseEntity<?> update(@RequestBody SstPersona personaUpdate, @PathVariable long codPersona) {
		
		SstPersona personaSave = new SstPersona();		
		
		try {
			personaSave= personaRepository.findByCodPersona(codPersona);
		} catch (DataAccessException e) {
			LOG.error("[update] "+e.getMessage().concat(": ").concat(e.getLocalizedMessage()));
			return new ResponseEntity<ResponseBean>(new ResponseBean("",HttpStatus.INTERNAL_SERVER_ERROR, personaUpdate),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(personaSave ==null) {
			
		}
		
		personaSave.setDesNombre(personaUpdate.getDesNombre());
		personaSave.setDesApellido(personaUpdate.getDesApellido());
		personaSave.setEdad(personaUpdate.getEdad());
		personaRepository.save(personaSave);
		
		return new ResponseEntity<ResponseBean>(new ResponseBean("",HttpStatus.CREATED,personaSave),HttpStatus.CREATED);
	}

}
