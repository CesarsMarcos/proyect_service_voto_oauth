package com.proempresa.proyectservicesvoto.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.proempresa.proyectservicesvoto.bean.SstCandidato;
import com.proempresa.proyectservicesvoto.message.response.ResponseBean;
import com.proempresa.proyectservicesvoto.message.response.ResponseErrorBean;
import com.proempresa.proyectservicesvoto.repository.CandidatoJpaRepository;


@RestController
@RequestMapping("/api")
//@CrossOrigin(origins="*",  methods= {RequestMethod.GET,RequestMethod.POST})
public class CandidatoController {

	private static final Logger LOG = LoggerFactory.getLogger(PersonaController.class);

	@Autowired
	CandidatoJpaRepository candidatoJpaRepository;

	@GetMapping("/candidato")
	@Transactional(readOnly = true)
	private ResponseEntity<?> get() {
		LOG.info("[CandidatoController - get ]");
		List<SstCandidato> listaCandidatos = candidatoJpaRepository.findAll();
		return new ResponseEntity<List<SstCandidato>>(listaCandidatos, HttpStatus.OK);
	}

	@PostMapping("/candidato")
	@Transactional
	private ResponseEntity<?> add(@RequestBody SstCandidato candidato) {
		LOG.info("[CandidatoController - add ]");
		SstCandidato candidatoNew = null;
		try {
			candidato.setImagen("avatar.jpg");
			candidatoNew = candidatoJpaRepository.save(candidato);
		} catch (Exception e) {
			return new ResponseEntity<ResponseBean>(
					new ResponseBean("Ocurrio ", HttpStatus.INTERNAL_SERVER_ERROR, null),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ResponseBean>(
				new ResponseBean("Se realizo el registro de " + candidatoNew.getDesCandidato() + " con exito.",
						HttpStatus.CONFLICT, candidatoNew),
				HttpStatus.CONFLICT);
	}

	@PostMapping("/candidado/upload")
	@Transactional
	private ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {
		LOG.info("[CandidatoController - upload ]");
		Map<String, Object> response = new HashMap<>();
		SstCandidato candidado = candidatoJpaRepository.findByCodCandidato(id);

		if (!archivo.isEmpty()) {
			String nombreArchivo = UUID.randomUUID().toString() + "_" + archivo.getOriginalFilename().replace(" ", "");
			Path rutaArchivo = Paths.get("D:\\uploads_img").resolve(nombreArchivo).toAbsolutePath();
			try {
				Files.copy(archivo.getInputStream(), rutaArchivo);
			} catch (IOException e) {
				response.put("mensaje", "Error al realizar la carga de la imagen");
				response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			if (candidado != null) {
				candidado.setImagen(nombreArchivo);
				candidatoJpaRepository.save(candidado);
			} else {
				return new ResponseEntity<ResponseErrorBean>(
						new ResponseErrorBean(HttpStatus.BAD_REQUEST, "Candidato no existe", "Candidato no existe"),
						HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<ResponseBean>(new ResponseBean("", HttpStatus.OK, candidado), HttpStatus.OK);
	}

	@GetMapping("uploads/img/{nombreImagen:.+}")
	public ResponseEntity<Resource> verImagen(@PathVariable String nombreImagen) {
		Path rutaArchivo = Paths.get("D:\\uploads_img").resolve(nombreImagen).toAbsolutePath();
		Resource recurso = null;

		try {
			recurso = new UrlResource(rutaArchivo.toUri());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!recurso.exists() && recurso.isReadable()) {
			throw new RuntimeException("No se pudo cargar la imagen " + nombreImagen);
		}

		HttpHeaders cabeceraHeaders = new HttpHeaders();
		cabeceraHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");

		return new ResponseEntity<Resource>(recurso, cabeceraHeaders, HttpStatus.OK);

	}

}
