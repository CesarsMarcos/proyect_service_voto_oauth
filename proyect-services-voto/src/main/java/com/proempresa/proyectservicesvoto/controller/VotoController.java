package com.proempresa.proyectservicesvoto.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.proempresa.proyectservicesvoto.bean.SstUsuario;
import com.proempresa.proyectservicesvoto.bean.SstVoto;
import com.proempresa.proyectservicesvoto.message.response.ResponseBean;
import com.proempresa.proyectservicesvoto.message.response.ResponseErrorBean;
import com.proempresa.proyectservicesvoto.repository.UsuarioJpaRepository;
import com.proempresa.proyectservicesvoto.repository.VotoJpaRepository;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins="*" ,methods= {RequestMethod.GET,RequestMethod.POST})
public class VotoController {
	
	public static final Logger LOG = LoggerFactory.getLogger(VotoController.class);
	
	@Autowired
	VotoJpaRepository votoJpaRepository;
	
	@Autowired
	UsuarioJpaRepository usuarioJpaRepository;
	
	@GetMapping("/voto")
	@Transactional(readOnly=true)
	private ResponseEntity<?> get (){
		LOG.info("[VotoController - get]");
		List<SstVoto> listVoto = null;
		try {
			listVoto = votoJpaRepository.findAll();
		} catch (Exception e) {
			return new ResponseEntity<List<SstVoto>>(listVoto,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<SstVoto>>(listVoto,HttpStatus.OK);
	}
	
	
	@PostMapping("/voto")
	@Transactional
	private ResponseEntity<?> add(@RequestBody SstVoto voto) {

		LOG.info("[VotoController - add]");
		SstVoto votoNew = null;
		SstUsuario usuarioVoto = null;
		Map<String, Object> response = new HashMap<>();
		try {
			
			String codPersonal= voto.getCodPersonal();
			usuarioVoto = usuarioJpaRepository.findByCodPersonal(codPersonal);
			
			 if(usuarioVoto.getIndVoto().equals("N")) {
				 voto.setIndBaja("N"); 
				 votoNew =  votoJpaRepository.save(voto);
				 
				 usuarioVoto.setIndVoto("S");
				 usuarioJpaRepository.save(usuarioVoto);
				 
				 return new ResponseEntity<ResponseBean>(new ResponseBean("Su Voto fue registrado con Exito !!", HttpStatus.CREATED, votoNew), HttpStatus.CREATED);
			 }else {
				 response.put("status", HttpStatus.CONFLICT);
				 response.put("message", "Usted ya realizo su voto !!");
				 return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
			 }
			 
		} catch (DataAccessException e) {
			return new ResponseEntity<ResponseErrorBean>(new ResponseErrorBean(HttpStatus.INTERNAL_SERVER_ERROR, "Error al Registrar el voto",e.getLocalizedMessage().concat(": ").concat(e.getMostSpecificCause().getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}
	
	

}
