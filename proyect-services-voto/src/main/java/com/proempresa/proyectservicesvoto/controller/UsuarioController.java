package com.proempresa.proyectservicesvoto.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.proempresa.proyectservicesvoto.repository.UsuarioJpaRepository;


@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins="*" ,methods= {RequestMethod.GET,RequestMethod.POST})
public class UsuarioController {
	
	@Autowired
	UsuarioJpaRepository usuarioJpa;

}
