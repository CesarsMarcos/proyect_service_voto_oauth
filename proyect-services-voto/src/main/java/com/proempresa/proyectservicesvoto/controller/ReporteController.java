package com.proempresa.proyectservicesvoto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.proempresa.proyectservicesvoto.bean.SstReporteCandidato;
import com.proempresa.proyectservicesvoto.message.response.ReporteVoto;
import com.proempresa.proyectservicesvoto.message.response.ResponseBean;
import com.proempresa.proyectservicesvoto.repository.VotoJpaRepository;
import com.proempresa.proyectservicesvoto.service.IReporteService;




@RestController
@RequestMapping("/api/")
@CrossOrigin(origins="*" ,methods= {RequestMethod.GET,RequestMethod.POST})
public class ReporteController {

	@Autowired
	IReporteService reporteService;
	
	@Autowired
	VotoJpaRepository votoJpaRepository;
	
	@GetMapping("/reportevoto")
	@Transactional(readOnly = true)
	private  ResponseEntity<?> getVoto (){
		 List<ReporteVoto> reporte= reporteService.getReporteVoto();
		return new ResponseEntity<ResponseBean>(new ResponseBean("LISTA DE VOTOS POR AGENCIA",HttpStatus.OK, reporte), HttpStatus.OK);
	}
	
	@GetMapping("/reportecandidato")
	@Transactional(readOnly=true)
	private ResponseEntity<?> getCandidato (){
		List<SstReporteCandidato> reporte  =  reporteService.getReporteCandidato();
		return new ResponseEntity<ResponseBean> (new ResponseBean("LISTA DE VOTOS POR CANDIDATO", HttpStatus.OK, reporte),HttpStatus.OK);
	}
	
	
}
