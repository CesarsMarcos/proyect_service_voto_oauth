package com.proempresa.proyectservicesvoto.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proempresa.proyectservicesvoto.bean.SstCandidato;



@Repository
public interface CandidatoJpaRepository  extends JpaRepository<SstCandidato,Serializable> {

	SstCandidato findByCodCandidato(Long id);

}
