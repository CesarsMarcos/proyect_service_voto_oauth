package com.proempresa.proyectservicesvoto.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proempresa.proyectservicesvoto.bean.Usuario;

public interface UsuarioRepository  extends JpaRepository<Usuario, Serializable>{

	Usuario findByUsername(String username);

}
