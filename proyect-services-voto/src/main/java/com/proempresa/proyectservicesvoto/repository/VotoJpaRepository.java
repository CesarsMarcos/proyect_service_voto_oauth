package com.proempresa.proyectservicesvoto.repository;

import java.io.Serializable;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proempresa.proyectservicesvoto.bean.SstVoto;

@Repository
public interface VotoJpaRepository extends  JpaRepository<SstVoto, Serializable> {

	SstVoto  findByUserAdd(String userAdd);

}
