package com.proempresa.proyectservicesvoto.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proempresa.proyectservicesvoto.bean.SstUsuario;



public interface UsuarioJpaRepository  extends JpaRepository<SstUsuario,Serializable>{
	
	
	public SstUsuario findByCodPersonal(String codPersonal);

	public SstUsuario findByUsername(String username);

	
}
