package com.proempresa.proyectservicesvoto.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proempresa.proyectservicesvoto.bean.SstPersona;


@Repository
public interface PersonaJpaRepository extends JpaRepository<SstPersona, Serializable>{

	SstPersona findByCodPersona(long codPersona);


}
