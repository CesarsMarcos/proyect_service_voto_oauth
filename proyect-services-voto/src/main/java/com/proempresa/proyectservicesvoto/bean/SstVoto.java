package com.proempresa.proyectservicesvoto.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="SST_VOTO")
public class SstVoto implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="CUR_VOTO")
	//@SequenceGenerator(sequenceName="SEC_SST_VOTO", allocationSize=1, name="CUR_VOTO")
	@Column(name="COD_VOTO")
	private Long codVoto;
	
	@Column(name = "COD_CANDIDATO")
	private String codCandidato;
	
	@Column(name="USER_ADD")
	private String userAdd;
	
	@Column (name = "DES_AGENCIA")
	private String desAgencia;
	
	
	@Column(name = "IND_BAJA")
	private String IndBaja;
	
	@Column(name = "COD_PERSONAL")
	private String codPersonal;
	
	
	@Column(name="FEC_ADD")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecAdd;
	
	@Column(name="FEC_UPDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecUpdate;

	@PrePersist
	protected void onCreate() {
		fecAdd = new Date();
	}
	@PreUpdate
	protected void onUpdate() {
		fecUpdate = new Date();
	}
	public Long getCodVoto() {
		return codVoto;
	}

	public void setCodVoto(Long codVoto) {
		this.codVoto = codVoto;
	}

	public String getIndBaja() {
		return IndBaja;
	}

	public void setIndBaja(String indBaja) {
		IndBaja = indBaja;
	}

	public String getCodCandidato() {
		return codCandidato;
	}
	public void setCodCandidato(String codCandidato) {
		this.codCandidato = codCandidato;
	}
	public String getUserAdd() {
		return userAdd;
	}
	public void setUserAdd(String userAdd) {
		this.userAdd = userAdd;
	}
	public String getDesAgencia() {
		return desAgencia;
	}
	public void setDesAgencia(String desAgencia) {
		this.desAgencia = desAgencia;
	}
	public Date getFecAdd() {
		return fecAdd;
	}
	public void setFecAdd(Date fecAdd) {
		this.fecAdd = fecAdd;
	}
	public Date getFecUpdate() {
		return fecUpdate;
	}
	public void setFecUpdate(Date fecUpdate) {
		this.fecUpdate = fecUpdate;
	}




	public String getCodPersonal() {
		return codPersonal;
	}
	public void setCodPersonal(String codPersonal) {
		this.codPersonal = codPersonal;
	}








	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
}
