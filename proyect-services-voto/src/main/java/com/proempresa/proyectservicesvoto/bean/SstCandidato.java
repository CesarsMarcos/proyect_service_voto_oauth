package com.proempresa.proyectservicesvoto.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SST_CANDIDATO")
public class SstCandidato implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUR_CANDIDATO")
	//@SequenceGenerator(sequenceName = "SEC_SST_CANDIDATO", allocationSize = 1, name = "CUR_CANDIDATO")
	@Column(name = "COD_CANDIDATO")
	private Long codCandidato;

	@Column(name = "DES_CANDIDATO")
	private String desCandidato;

	@Column(name = "DES_AGENCIA")
	private String desAgencia;

	@Column(name = "DES_DNI")
	private String desDni;
	
	private String imagen;

	@Column(name = "USER_ADD")
	private String userAdd;

	@Column(name = "IND_BAJA")
	private String indBaja;

	@Column(name = "FEC_ADD")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecAdd;

	@Column(name = "FEC_UPDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecUpdate;

	@PrePersist
	protected void onCreate() {
		fecAdd = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		fecUpdate = new Date();
	}

	public Long getCodCandidato() {
		return codCandidato;
	}

	public void setCodCandidato(Long codCandidato) {
		this.codCandidato = codCandidato;
	}

	public String getDesCandidato() {
		return desCandidato;
	}

	public void setDesCandidato(String desCandidato) {
		this.desCandidato = desCandidato;
	}

	public String getDesAgencia() {
		return desAgencia;
	}

	public void setDesAgencia(String desAgencia) {
		this.desAgencia = desAgencia;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getUserAdd() {
		return userAdd;
	}

	public void setUserAdd(String userAdd) {
		this.userAdd = userAdd;
	}

	public String getIndBaja() {
		return indBaja;
	}

	public void setIndBaja(String indBaja) {
		this.indBaja = indBaja;
	}

	public Date getFecAdd() {
		return fecAdd;
	}

	public void setFecAdd(Date fecAdd) {
		this.fecAdd = fecAdd;
	}

	public Date getFecUpdate() {
		return fecUpdate;
	}

	public void setFecUpdate(Date fecUpdate) {
		this.fecUpdate = fecUpdate;
	}


	public String getDesDni() {
		return desDni;
	}

	public void setDesDni(String desDni) {
		this.desDni = desDni;
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
