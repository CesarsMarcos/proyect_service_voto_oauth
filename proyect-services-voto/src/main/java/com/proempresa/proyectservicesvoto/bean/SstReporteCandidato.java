package com.proempresa.proyectservicesvoto.bean;

public class SstReporteCandidato {
	
	private String  desCandidato;
	private String desAgencia;
	private String sumVotos;
	
	public String getDesCandidato() {
		return desCandidato;
	}
	public void setDesCandidato(String desCandidato) {
		this.desCandidato = desCandidato;
	}
	public String getDesAgencia() {
		return desAgencia;
	}
	public void setDesAgencia(String desAgencia) {
		this.desAgencia = desAgencia;
	}
	public String getSumVotos() {
		return sumVotos;
	}
	public void setSumVotos(String sumVotos) {
		this.sumVotos = sumVotos;
	}
	
	
	
	
	
}
