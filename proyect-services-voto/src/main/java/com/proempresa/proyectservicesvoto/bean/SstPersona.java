package com.proempresa.proyectservicesvoto.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SST_PERSONA")
public class SstPersona implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// @SequenceGenerator(sequenceName = "SEC_TRO_PERSONA", allocationSize = 1, name
	// = "CUS_TRO")
	@Column(name = "COD_PERSONA")
	private long codPersona;
	@Column(name = "DES_NOMBRE")
	private String desNombre;
	@Column(name = "DES_APELLIDO")
	private String desApellido;
	@Column(name = "EDAD")
	private String edad;
	@Column(name = "FEC_ADD")
	@Temporal(TemporalType.TIMESTAMP)
	private Date FecAdd;
	@Column(name = "FEC_UPDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date FecUpdate;
	@Column(name = "IND_BAJA")
	private String IndBaja;

	@PrePersist
	protected void onCreate() {
		FecAdd = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		FecUpdate = new Date();
	}

	public long getCodPersona() {
		return codPersona;
	}

	public void setCodPersona(long codPersona) {
		this.codPersona = codPersona;
	}

	public String getDesNombre() {
		return desNombre;
	}

	public void setDesNombre(String desNombre) {
		this.desNombre = desNombre;
	}

	public String getDesApellido() {
		return desApellido;
	}

	public void setDesApellido(String desApellido) {
		this.desApellido = desApellido;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public Date getFecAdd() {
		return FecAdd;
	}

	public void setFecAdd(Date fecAdd) {
		FecAdd = fecAdd;
	}

	public Date getFecUpdate() {
		return FecUpdate;
	}

	public void setFecUpdate(Date fecUpdate) {
		FecUpdate = fecUpdate;
	}

	public String getIndBaja() {
		return IndBaja;
	}

	public void setIndBaja(String indBaja) {
		IndBaja = indBaja;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
