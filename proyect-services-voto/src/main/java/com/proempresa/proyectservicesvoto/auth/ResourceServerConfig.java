package com.proempresa.proyectservicesvoto.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig  extends ResourceServerConfigurerAdapter{
	
	private Logger LOG = LoggerFactory.getLogger(ResourceServerConfig.class);
	
	//PROTEGER POINTS DE SERVICIO
	@Override
	public void configure(HttpSecurity http) throws Exception {
		LOG.info("[ResourceServerConfig - configure]");
		http.authorizeRequests()
		.antMatchers(HttpMethod.GET,"/api/candidato").permitAll()
		.anyRequest().authenticated();
	}
	
}
