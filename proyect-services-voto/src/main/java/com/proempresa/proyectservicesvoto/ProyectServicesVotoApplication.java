package com.proempresa.proyectservicesvoto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectServicesVotoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectServicesVotoApplication.class, args);
	}

}
