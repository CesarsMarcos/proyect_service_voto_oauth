package com.proempresa.proyectservicesvoto.service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.proempresa.proyectservicesvoto.bean.SstUsuario;


public class UserPrincipal implements UserDetails {

	private Long cod;
	private String desUsuario;
	private String codAuxiliar;
	@JsonIgnore
	private String desPassword;
	private String indBaja;
	private Collection<? extends GrantedAuthority> authorities;

	public UserPrincipal(Long cod, String desUsuario, String codAuxiliar, String desPassword, String indBaja,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.cod = cod;
		this.desUsuario = desUsuario;
		this.codAuxiliar = codAuxiliar;
		this.desPassword = desPassword;
		this.indBaja = indBaja;
		this.authorities = authorities;
	}

	public static UserPrincipal build(SstUsuario usuario) {

		List<GrantedAuthority> authorities = usuario.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getDesRole())).collect(Collectors.toList());
		return new UserPrincipal(usuario.getCod(), usuario.getUsername(), usuario.getCodAuxiliar(),
				usuario.getPassword(), usuario.getIndBaja(), authorities);
	}

	public Long getCod() {
		return cod;
	}

	public void setCod(Long cod) {
		this.cod = cod;
	}

	public String getDesUsuario() {
		return desUsuario;
	}

	public void setDesUsuario(String desUsuario) {
		this.desUsuario = desUsuario;
	}

	public String getCodAuxiliar() {
		return codAuxiliar;
	}

	public void setCodAuxiliar(String codAuxiliar) {
		this.codAuxiliar = codAuxiliar;
	}

	public String getDesPassword() {
		return desPassword;
	}

	public void setDesPassword(String desPassword) {
		this.desPassword = desPassword;
	}

	public String getIndBaja() {
		return indBaja;
	}

	public void setIndBaja(String indBaja) {
		this.indBaja = indBaja;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return getDesPassword();
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return getDesUsuario();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {

		if (getIndBaja() == "S") {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
