package com.proempresa.proyectservicesvoto.service;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Component;

import com.proempresa.proyectservicesvoto.bean.SstReporteCandidato;
import com.proempresa.proyectservicesvoto.message.response.ReporteVoto;


@Component
public class ReporteServiceIm implements IReporteService {

	@PersistenceContext
	private EntityManager entityManager;

	

	@Override
	public List<ReporteVoto> getReporteVoto() {
		List<ReporteVoto> listReporteVoto = new ArrayList<>();
		try {
			StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("SP_SST_REPORTE_VOTO_AGENCIA");
					
			storedProcedure.registerStoredProcedureParameter("vo_reporte_voto", ResultSet.class, ParameterMode.REF_CURSOR);
			
			storedProcedure.execute();
			ResultSet vo_reporte_voto = (ResultSet) storedProcedure.getOutputParameterValue("vo_reporte_voto");

			while (vo_reporte_voto.next()) {
				ReporteVoto reporte = new ReporteVoto();
				reporte.setAgencia(vo_reporte_voto.getString(1));
				reporte.setCantidadVoto(vo_reporte_voto.getString(2));
				
				listReporteVoto.add(reporte);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listReporteVoto;
	}

	@Override
	public List<SstReporteCandidato> getReporteCandidato() {
		
		List<SstReporteCandidato>  listReporteCandidato = new ArrayList<>();
		try {
			StoredProcedureQuery storedProcedure=entityManager.createStoredProcedureQuery("SP_SST_REPORTE_VOTO_CANDIDATO");
					
			storedProcedure.registerStoredProcedureParameter("vo_reporte_candidato", ResultSet.class, ParameterMode.REF_CURSOR);
			
			storedProcedure.execute();
			ResultSet vo_reporte_candidato = (ResultSet) storedProcedure.getOutputParameterValue("vo_reporte_candidato");
			
			while (vo_reporte_candidato.next()) {
				SstReporteCandidato reporte  = new SstReporteCandidato();
				reporte.setDesCandidato(vo_reporte_candidato.getString(1));
				reporte.setDesAgencia(vo_reporte_candidato.getString(2));
				reporte.setSumVotos(vo_reporte_candidato.getString(3));
				
				listReporteCandidato.add(reporte);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listReporteCandidato;
	}

}
