package com.proempresa.proyectservicesvoto.service;


import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proempresa.proyectservicesvoto.bean.SstUsuario;
import com.proempresa.proyectservicesvoto.repository.UsuarioJpaRepository;



@Service
public class UsuarioSevice implements UserDetailsService {

	private final static Logger LOG = LoggerFactory.getLogger(UserDetailsService.class);

	@Autowired
	private UsuarioJpaRepository usuarioRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		SstUsuario usuario = usuarioRepository.findByUsername(username);

		if (usuario == null) {
			LOG.error("El usuario no se encuentra registrado "+username);
			throw new UsernameNotFoundException("El usuario no se encuentra registrado "+username);
		}
	
		List<GrantedAuthority> authorities = usuario.getRoles()
				.stream()
				.map(role -> new SimpleGrantedAuthority(role.getDesRole()))
				.collect(Collectors.toList());

		return new User(usuario.getUsername(), usuario.getPassword(), true, true, true, true,authorities);
	}

}
