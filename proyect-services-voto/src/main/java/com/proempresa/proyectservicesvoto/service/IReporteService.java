package com.proempresa.proyectservicesvoto.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.proempresa.proyectservicesvoto.bean.SstReporteCandidato;
import com.proempresa.proyectservicesvoto.message.response.ReporteVoto;


@Service
public interface IReporteService {
	
	public List<ReporteVoto> getReporteVoto ();
	public List<SstReporteCandidato> getReporteCandidato();
	

}
